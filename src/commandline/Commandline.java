/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commandline;

import java.util.* ;

public class Commandline {

    static Scanner kb = new Scanner(System.in);
    
    static String Rec_User ;
    static String Rec_Pass ;
    static boolean foundUser = false , foundPass = false ;
    
    static ArrayList <User> List = new ArrayList <> () ;
    
    static User wait;
    static boolean foundEditPass = false;
    
    static String a1, a2, a3, a4, e1, e2, e3, e4;
    static double a5, a6, e5, e6;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        load() ;
        sayLogin() ;
    }
     public static void load() {
        List.add(new User("admin","admin","Mister","HeartRocker",180,70));
        List.add(new User("admin1","admin1","Mister","Cigarettes",175,60));
    }
     public static void showErrorLoginUser() {
		System.err.println("Username is incorrect.");
	}

	public static void showErrorLoginPass() {
		System.err.println("Password is incorrect.");
	}

	public static void showSuccess() {
		System.out.println("Success.");
	}

	public static void showPleaseChoose() {
		System.out.print("Please choose : ");
	}

	public static void showErrorInput() {
		System.err.println("Input didn't match.");
	}

	public static void showErrorAddUser() {
		System.err.println("If Username has been exist. This message will show.");
	}

	public static void showErrorZero() {
		System.err.println("Cannot add or edit with a zero");
	}

	public static void showErrorBlank() {
		System.err.println("Cannot add or edit with a blank");
	}

	public static void showErrorIndex() {
		System.err.println("Some field didn't fill.");
	}

	public static void showErrorShowInput() {
		System.err.println("Weight must be number.");
	}

	public static void showErrorWeight() {
		System.err.println("Weight must be number.");
	}

	public static void showErrorHeight() {
		System.err.println("Height must be number.");
	}

	public static void showErrorEditPass() {
		System.out.println("Password didn't match");
	}

	// for Login menu
	public static void sayLogin() {
		System.out.println("Please input your username and password");
		System.out.print("Username : ");
		Rec_User = kb.next();
		System.out.print("Password : ");
		Rec_Pass = kb.next();
		checkLogin(Rec_User, Rec_Pass);
	}

	public static void checkLogin(String user, String password) {
		for (User a : List) {
			if (Rec_User.equals(a.Username)) {
				foundUser = true;
				if (Rec_Pass.equals(a.getPassword())) {
					foundPass = true;
					break;
				} else {
					foundPass = false;
					break;
				}
			} else {
				foundUser = false;
			}
		}
		checkLoginFound();
	}

	public static void checkLoginFound() {
		if (foundUser == true && foundPass == true) {
			showSuccess();
			sayMenu();
		} else if (foundUser == true && foundPass == false) {
			showErrorLoginPass();
			sayLogin();
		} else {
			showErrorLoginUser();
			sayLogin();
		}
	}

	// for menu panel
	public static void sayMenu() {
		System.out.println("Menu Panel");
		System.out.println("1. Add");
		System.out.println("2. Show");
		System.out.println("3. Edit");
		System.out.println("4. Log Out");
		System.out.println("5. Exit");
		System.out.println("");
		checkMenu();
	}

	public static void checkMenu() {
		showPleaseChoose();
		String input = kb.next();
		switch (input) {
		case "1":
			sayAdd();
			break;
		case "2":
			sayShow();
			break;
		case "3":
			sayEditCheckPass();
			break;
		case "4":
			logOut();
			break;
		case "5":
			System.exit(0);
			break;
		default:
			showErrorInput();
			sayMenu();
		}
	}

	// for add panel
	public static void sayAdd() {
		System.out.println("Please input information");
		System.out.println("1. Username 2. Password 3. Firstname 4. Lastname 5. Weight 6. Height");
		System.out.println("(0) back");
		add();
	}

	public static void chooseAddUser() {
		System.out.print("Username : ");
		a1 = kb.next();
		if (a1.equals("0")) {
			sayMenu();
		} else {
			for (User a : List) {
				if (a.getUsername().equals(a1)) {
					showErrorAddUser();
					chooseAddUser();
				}
			}
			System.out.println("Username can use.");
		}

	}
        public static void chooseAddPass() {
		System.out.print("Password : ");
		a2 = kb.next();
		if (a2.equals("0"))
			sayMenu();
	}

	public static void chooseAddFname() {
		System.out.print("Firstname : ");
		a3 = kb.next();
		if (a3.equals("0"))
			sayMenu();
	}

	public static void chooseAddLname() {
		System.out.print("Lastname : ");
		a4 = kb.next();
		if (a4.equals("0"))
			sayMenu();
	}

	public static void chooseAddWeight() {
		boolean check = false;
		while (!check) {
			try {
				System.out.print("Weight (cannot be zero) : ");
				a5 = kb.nextDouble();
				if (a5 == 0) {
					sayMenu();
					break;
				}
				check = true;
			} catch (InputMismatchException e) {
				showErrorWeight();
				kb.next();
			}
		}
		System.out.println("Weight can use.");
	}

	public static void chooseAddHeight() {
		boolean check = false;
		while (!check) {
			try {
				System.out.print("Height (cannot be zero) : ");
				a6 = kb.nextDouble();
				if (a6 == 0) {
					sayMenu();
					break;
				}
				check = true;
			} catch (InputMismatchException e) {
				showErrorHeight();
				kb.next();
			}
		}
		System.out.println("Height can use.");
	}

	public static void add() {
		chooseAddUser();
		chooseAddPass();
		chooseAddFname();
		chooseAddLname();
		chooseAddWeight();
		chooseAddHeight();
		addSave();
	}

	public static void addSave() {
		System.out.println("(0) back / (Y) save");
		showPleaseChoose();
		String input = kb.next();
		if (input.equalsIgnoreCase("Y")) {
			List.add(new User(a1, a2, a3, a4, a5, a6));
			showSuccess();
			sayMenu();
		} else if (input.equals("0")) {
			sayAdd();
		} else
			addSave();

	}

	public static void addSetDefault() {
		a1 = "";
		a2 = "";
		a3 = "";
		a4 = "";
		a5 = 0;
		a6 = 0;
	}

	// for show panel
	public static void sayShow() {
		System.out.println("Show Panel");
		for (int i = 0; i < List.size(); i++) {
			System.out.println(i + 1 + ". " + List.get(i).Username);
		}
		System.out.println("(0) back");
		show();
	}

	public static void checkShow(int input) {
		for (int i = 0; i < List.size(); i++) {
			if (input == i + 1) {
				System.out.println(List.get(i));
				sayShow();
				break;
			} else if (input == 0) {
				sayMenu();
				break;
			} else if (input > List.size()) {
				showErrorInput();
				sayShow();
				break;
			}
		}
		showErrorInput();
		sayShow();
	}

	public static void show() {
		boolean check = false;
		int input;
		while (!check) {
			try {
				showPleaseChoose();
				input = kb.nextInt();
				checkShow(input);

			} catch (InputMismatchException e) {
				showErrorShowInput();
				kb.next();
			}
		}
	}

	// for edit panel
	public static void sayEditCheckPass() {
		editCheckPass();
		showPleaseChoose();
		String input = kb.next();
		if (input.equals("0")) {
			sayMenu();
		} else {
			editPassFound(input);
		}
	}

	public static void editCheckPass() {
		System.out.println("Please input your password");
		System.out.println("(0) back");
	}

	public static void editPassFound(String input) {
		for (User a : List) {
			if (input.equals(a.getPassword()) && input.equals(Rec_Pass)) {
				wait = new User(a.getUsername(), a.getPassword(), a.getFirstname(), a.getLastname(), a.getWeight(), a.getHeight());
				sayEdit();
			}
		}
		if (foundEditPass == false) {
			showErrorEditPass();
			sayEditCheckPass();
		}
	}

	public static void sayEdit() {
		chooseEdit();
		String input = kb.next();
		if (input.equals("0")) {
			sayMenu();
		} else if (input.equals("2")) {
			chooseEditPass();
			sayEdit();
		} else if (input.equals("3")) {
			chooseEditFname();
			sayEdit();
		} else if (input.equals("4")) {
			chooseEditLname();
			sayEdit();
		} else if (input.equals("5")) {
			chooseEditWeight();
			sayEdit();
		} else if (input.equals("6")) {
			chooseEditHeight();
			sayEdit();
		} else if (input.equalsIgnoreCase("Y")) {
			editSave();
		} else {
			showErrorInput();
			sayEdit();
		}

	}

	public static void chooseEdit() {
		System.out.println("Edit Panel");
		System.out.println(
				"1. Username: " + wait.getUsername() + " 2. Password: " + wait.getPassword() + " 3. Firstname: " + wait.getFirstname()
						+ " 4. Lastname: " + wait.getLastname() + " 5. Weight: " + wait.getWeight() + " 6. Height: " + wait.getHeight());
		System.out.println("(0) back / (Y)save");
		System.err.println("Username can't change!");
	}

	public static void editSave() {
		for (User a : List) {
			if (a.getUsername().equals(wait.Username)) {
				a.setPassword(wait.getPassword());
				a.setFirstname(wait.getFirstname());
				a.setLastname(wait.getLastname());
				a.setWeight(wait.getWeight());
				a.setHeight(wait.getHeight());
			}
		}
		showSuccess();
		sayMenu();
	}

	public static void chooseEditWeight() {
		boolean check = false;
		while (!check) {
			try {
				System.out.print("weight : ");
				e5 = kb.nextDouble();
				if (e5 == 0) {
					sayEdit();
					break;
				}
				wait.setWeight(e5);
				check = true;
			} catch (InputMismatchException e) {
				showErrorWeight();
				kb.next();
			}
		}
		System.out.println("Weight can use.");
	}

	public static void chooseEditHeight() {
		boolean check = false;
		while (!check) {
			try {
				System.out.print("height : ");
				e6 = kb.nextDouble();
				if (e6 == 0) {
					sayEdit();
					break;
				}
				wait.setHeight(e6);
				check = true;
			} catch (InputMismatchException e) {
				showErrorHeight();
				kb.next();
			}
		}
		System.out.println("Height can use.");
	}

	public static void chooseEditPass() {
		System.out.print("password : ");
		e2 = kb.next();
		if (e2.equals("0"))
			sayEdit();
		wait.setPassword(e2);
	}

	public static void chooseEditFname() {
		System.out.print("firstname : ");
		e3 = kb.next();
		if (e3.equals("0"))
			sayEdit();
		wait.setFirstname(e3);
	}

	public static void chooseEditLname() {
		System.out.print("lastname : ");
		e4 = kb.next();
		if (e4.equals("0"))
			sayEdit();
		wait.setLastname(e4);
	}

	// for logout
	public static void logOut() {
		System.out.println("Do you want to logout?(Y/N)");
		showPleaseChoose();
		String input = kb.next();
		if (input.equalsIgnoreCase("Y")) {
			sayLogin();
		} else if (input.equalsIgnoreCase("N")) {
			sayMenu();
		} else
			logOut();

	}

}
class User {
    
    String Username , Password , Firstname , Lastname ;
    double Weight , Height ;

    public User(String Username , String Password , String Firstname , String Lastname , double Weight , double Height){
        this.Username = Username ;
        this.Password = Password ;
        this.Firstname = Firstname ;
        this.Lastname = Lastname ;
        this.Weight = Weight ;
        this.Height = Height ;
    }
    public String toString() {
	String user = "Username : " + Username ;
	String pass = "\nPassword : " + Password;
	String fname = "\nFirstname : " + Firstname;
	String lname = "\nLastname : " + Lastname;
	String weight = "\nWeight : " + Double.toString(Weight);
	String height = "\nHeight : " + Double.toString(Height);
	return user + pass + fname + lname + weight + height ;
    }
    public void setUsername(String Username) {
        this.Username = Username;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public void setFirstname(String Firstname) {
        this.Firstname = Firstname;
    }

    public void setLastname(String Lastname) {
        this.Lastname = Lastname;
    }

    public void setWeight(double Weight) {
        this.Weight = Weight;
    }

    public void setHeight(double Height) {
        this.Height = Height;
    }
    
    public String getUsername() {
        return Username;
    }

    public String getPassword() {
        return Password;
    }

    public String getFirstname() {
        return Firstname;
    }

    public String getLastname() {
        return Lastname;
    }

    public double getWeight() {
        return Weight;
    }

    public double getHeight() {
        return Height;
    }

}
